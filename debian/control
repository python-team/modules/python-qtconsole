Source: python-qtconsole
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Picca Frédéric-Emmanuel <picca@debian.org>,
 Julian Gilbey <jdg@debian.org>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc <!nodoc>,
 python3-all,
 python3-ipykernel,
 python3-ipython (>= 7),
 python3-jupyter-client,
 python3-jupyter-core,
 python3-packaging,
 python3-pygments,
 python3-pyqt5,
 python3-pyqt5.qtsvg,
 python3-qtpy (>= 2.4.0),
 python3-setuptools,
 python3-sphinx <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>,
 python3-traitlets,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-qtconsole
Vcs-Git: https://salsa.debian.org/python-team/packages/python-qtconsole.git
Homepage: https://jupyter.org

Package: python3-qtconsole
Architecture: all
Depends:
 python3-ipykernel,
 python3-ipython (>= 7),
 ${misc:Depends},
 ${python3:Depends},
Description: Jupyter - Qt console (Python 3)
 IPython can be used as a replacement for the standard Python shell,
 or it can be used as a complete working environment for scientific
 computing (like Matlab or Mathematica) when paired with the standard
 Python scientific and numerical tools. It supports dynamic object
 introspections, numbered input/output prompts, a macro system,
 session logging, session restoring, complete system shell access,
 verbose and colored traceback reports, auto-parentheses, auto-quoting,
 and is embeddable in other Python programs.
 .
 This package contains the qtconsole library for the Python 3
 interpreter.  It does not depend on any Python Qt packages; it is the
 responsibility of the depending package to do that.  See
 /usr/share/doc/python3-qtconsole/README.Debian.gz for more
 information on this.

Package: jupyter-qtconsole
Architecture: all
Section: interpreters
Depends:
 jupyter-core,
 python3-pyqt5,
 python3-pyqt5.qtsvg,
 python3-qtconsole (= ${source:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: Jupyter - Qt console (binaries)
 IPython can be used as a replacement for the standard Python shell,
 or it can be used as a complete working environment for scientific
 computing (like Matlab or Mathematica) when paired with the standard
 Python scientific and numerical tools. It supports dynamic object
 introspections, numbered input/output prompts, a macro system,
 session logging, session restoring, complete system shell access,
 verbose and colored traceback reports, auto-parentheses,
 auto-quoting, and is embeddable in other Python programs.
 .
 This package contains the qtconsole binaries for the Python 3
 interpreter.

Package: python-qtconsole-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Jupyter - Qt console (documentation)
 IPython can be used as a replacement for the standard Python shell,
 or it can be used as a complete working environment for scientific
 computing (like Matlab or Mathematica) when paired with the standard
 Python scientific and numerical tools. It supports dynamic object
 introspections, numbered input/output prompts, a macro system,
 session logging, session restoring, complete system shell access,
 verbose and colored traceback reports, auto-parentheses, auto-quoting,
 and is embeddable in other Python programs.
 .
 This package contains the qtconsole library documentation.
Build-Profiles: <!nodoc>
Multi-Arch: foreign
